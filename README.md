Example 6: Simulating a Service Queue
========


This is an example that illustrates the use of Java’s `Queue<E>` interface. We instantiate a `Queue<E>` using the `LinkedList<E>` implementation of the interface.

### What is a Queue?

A **queue** is a datatype that supports, essentially, two methods: a method to add an element to the queue and a method to remove an element from the queue. 

Elements are added to the tail of the queue (i.e., at the end). Elements are removed from the front of the queue (i.e., from the beginning). A queue is also called a FIFO (first-in first-out) structure where the first element to be added is the first element to be removed.

A queue is a datatype that is often used because it models many physical systems.

### What is a Service Queue?

We consider a simple system where jobs arrive and need to be processed by a server. One concrete example is a bank, where the “jobs” are customers and the “server” is a bank’s teller.

In this example, we consider a system where jobs may arrive at random times but the separation time between two job arrivals is a random variable that is drawn from an [exponential distribution](http://en.wikipedia.org/wiki/Exponential_distribution) with rate parameter *lambda*. We may assume that *lambda* jobs arrive every minute on average. The amount of time taken to complete a job is a different random variable that is also exponentially distributed with rate parameter *mu*. Thus, on average, *mu* jobs can be completed by the server every minute. For the server to be able to complete all jobs, we must have *mu > lambda*.

Our program models such a queue, which is also known as an [M/M/1 queue](http://en.wikipedia.org/wiki/M/M/1_queue). We would like to verify that the server utilization is *lambda/mu* as theoretical analysis predicts.

### What is an Interface? 

We will describe an interface through an example. A queue may be represented using either an `array`, or an `ArrayList`, or a `LinkedList`. We only care about whether we can add and remove jobs in a manner that is consistent with the definition of a queue.

If someone wants to use a queue, they often need not care whether the actual implementation uses one of three choices mentioned above. They only want to use a queue. To separate concerns of design from implementation, Java allows us to declare interfaces. An interface is a group of related methods with empty bodies.

For example, we can define the `Queue<E>` interface as follows:
```java
interface Queue<E> {
	// method to add an element to the end of a queue
	void add(E element);

	// method to remove the element at the beginning of the queue
	E remove( );

	// method to get the size of the queue
	// because we cannot remove from an empty queue
	int size( );
}
```
In the above code snippet `E` represents the datatype of the objects we want to queue. We could declare a queue of integers using the following statement: `Queue<Int> intQ;`

An interface does not include an implementation of any of the methods. It is possible to declare an object to be of type `Queue` but we cannot create an object of type `Queue` because there is no implementation for the specified methods.

A class like `LinkedList<E>` can then implement the `Queue<E>` interface by implementing all the methods specified in the interface:
```java
public class LinkedList<E> implements Queue<E> {
	… // various methods including
	
	public void add(E element) {
		…
	}

	public E remove( ) {
		…
	}

	public int size( ) {
		…
	}
	… 
}
```

We can then create an actual queue with a statement like:
```java
Queue<Job> jobQueue = new LinkedList<Job>();
```

### Examining the Provided Source Code

Now one can explore the provided source code. The queue we use is created in `PoissonQueue.java` line 50:
```java
// This is the queue of jobs that need to be serviced.
// We use a LinkedList as the actual queue implementation.
Queue<Job> q = new LinkedList<Job>();
```
and is used in a few places.

Jobs are added to this queue, for example, on line 96:
```java
// add the job to the queue.
q.add(newJob);
```

Jobs are removed from the queue on line 110:
```java
// a job should have finished.
// let us remove the job from the queue.
Job currJob = q.remove();
```

Examine the source code to learn a few other tricks and methods that you may want to reuse later, such as random number generation.

### What is a Stack?

Although not critical to this example, a **stack** is a datatype that is similar to a queue except that the `add( )` method would add to the top of a stack and the `remove( )` method would remove the element at the top of a stack, resulting in LIFO (last in, first out) behaviour.

### Suggested Reading
* Oracle Java tutorial on [Interface](http://docs.oracle.com/javase/tutorial/java/concepts/interface.html)
* Documentation on [Queue](http://docs.oracle.com/javase/7/docs/api/java/util/Queue.html)
* Documentation on [LinkedList](http://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html)
* [Stacks and Queues](https://dl.dropboxusercontent.com/u/567187/EECE%20210/Java/StacksAndQueues.pdf), including [this video](http://www.youtube.com/playlist?list=PL908CE100665DC1BE).
