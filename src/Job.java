/**
 * This class represents a job to be done. We model a job very simply: a job has
 * an arrival time (when did the job arrive) and a service time (the time needed
 * to complete the job)
 * 
 * @author Sathish Gopalakrishnan
 * 
 */

public class Job {
    private double serviceTime; // how much time is needed to complete the job
    private double arrivalTime; // when did the job arrive

    /**
     * 
     * @param arrivalTime When the job arrived
     * @param serviceTime The amount of time needed to complete the job
     */
    public Job(double arrivalTime, double serviceTime) {
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    /**
     * 
     * @return the time the job arrived at
     */
    public double getArrivalTime() {
        return arrivalTime;
    }

    /**
     * 
     * @return the amount of time it takes to complete the job
     */
    public double getServiceTime() {
        return serviceTime;
    }

}
