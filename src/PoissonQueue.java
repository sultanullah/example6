/*************************************************************************
 * 
 * Simulate a queue where arrivals and departures are Poisson processes with
 * arrival rate lambda and service rate mu.
 * 
 * We assume the inter-arrival and service times are independent.
 * 
 * 
 *************************************************************************/

import java.util.Queue;
import java.util.LinkedList;

public class PoissonQueue {

    /**
     * 
     * This methods simulates a server system with one queue. The time between
     * two job arrivals is modeled as an exponentially distributed random
     * variable with rate lambda. The time taken to service a job is modeled as
     * an exponentially distributed random variable with rate mu.
     * 
     * We would like to verify if the system utilization is lambda/mu as theory
     * would predict.
     * 
     * @param args
     *            Two arguments, both doubles: lambda is the arrival rate of
     *            jobs and mu is the service rate of jobs.
     */
    public static void main(String[] args) {

        // The arguments to this program are the arrival rate and service rate.
        // If a sufficient number of arguments are not provided then exit.
        if (args.length < 2) {
            System.out.println("Insufficient arguments!");
            return;
        }

        // We seem to have the correct number of arguments.
        double lambda = Double.parseDouble(args[0]); // arrival rate
        double mu = Double.parseDouble(args[1]); // service rate

        // We are going to simulate the queue for a certain number of jobs.
        int jobsToSimulate = 10000;

        // Some statistics that we want to keep track of.
        double busyTime = 0;
        double prevDeparture = 0;

        // This is the queue of jobs that need to be serviced.
        // We use a LinkedList as the actual queue implementation.
        Queue<Job> q = new LinkedList<Job>();

        // When does the first job arrive?
        double nextArrival = StdRandom.exp(lambda); // time of next arrival

        // In the absence of any information, set the departure time (when a job
        // completes) to infinity.
        double nextDeparture = Double.POSITIVE_INFINITY; // time of next
                                                         // departure

        // Histogram object.
        // Allows us to visualize wait times.
        // See Histogram.java for implementation details.
        Histogram hist = new Histogram(100);

        // Simulate an M/M/1 queue as long as we are not done with the required
        // number of jobs.
        // Also, after the required number of jobs have been queued, we will
        // have to wait for the queue to clear.
        while (jobsToSimulate > 0 || q.size() != 0) {

            // it's an arrival
            // let us create a new job
            if (nextArrival <= nextDeparture) {
                // create a new job
                Job newJob = new Job(nextArrival, StdRandom.exp(mu));

                // increment busy time. This tells us how long the server has
                // been busy.
                // We wait for all jobs to finish so we can add all service
                // times.
                busyTime += newJob.getServiceTime();

                // reduce the number of jobs to simulate.
                jobsToSimulate--;

                // What if the queue is empty when a new job arrives.
                // The server can work on it at once.
                // Set the next departure time to current time (nextArrival) +
                // the service time of the new job.
                if (q.isEmpty())
                    nextDeparture = nextArrival + newJob.getServiceTime();

                // add the job to the queue.
                q.add(newJob);

                if (jobsToSimulate == 0)
                    // If there are no more jobs to simulate then set
                    // nextArrival to the distant future.
                    nextArrival = Double.POSITIVE_INFINITY;
                else
                    // Set the next arrival to have a separation that is an
                    // exponentially distributed random variable.
                    nextArrival += StdRandom.exp(lambda);
            }

            // it's a departure
            else {
                // a job should have finished.
                // let us remove the job from the queue.
                Job currJob = q.remove();

                // let us compute the flow time of the job that just finished
                // (total time since the job arrived)
                double flowTime = nextDeparture - currJob.getArrivalTime();

                // display the wait time - if needed
                // StdOut.printf("Wait = %6.2f, queue size = %d\n", flowTime,
                // q.size());

                // let us add the data point to the histogram
                hist.addDataPoint(Math.min(100, (int) (Math.round(flowTime))));
                hist.draw();

                // store when the previous job finished before moving on to the
                // next job in the queue
                prevDeparture = nextDeparture;
                if (q.isEmpty()) {
                    nextDeparture = Double.POSITIVE_INFINITY;
                } else
                    // set the next departure time based on the job at the head
                    // of the queue
                    // we use peek( ) to get the job at the head of the queue
                    nextDeparture += q.peek().getServiceTime();

            }
        }

        // The system utilization is the fraction of time for which the server
        // was busy. When the simulation ends, prevDeparture holds the time at
        // which the last job finished.
        System.out.println("System utilization: " + busyTime / prevDeparture);

    }
}
